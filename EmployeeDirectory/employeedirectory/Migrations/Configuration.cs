﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using EmployeeDirectory.DAL;
using EmployeeDirectory.Models;

namespace EmployeeDirectory.Migrations
{


    public sealed class Configuration : DbMigrationsConfiguration<EmployeeDirectory.DAL.CompanyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "EmployeeDirectory.DAL.CompanyContext";
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(CompanyContext context)
        {

            //if (!(context.Users.Any(u => u.UserName == "admin@employeeDirectory.com")))
            //{
            //    var userStore = new UserStore<ApplicationUser>(context);
            //    var userManager = new UserManager<ApplicationUser>(userStore);
            //    var userToInsert = new ApplicationUser { UserName = "admin@employeeDirectory.com" };
            //    userManager.Create(userToInsert, "admin1!");
            //}

            //if (!(context.Users.Any(u => u.UserName == "user@employeeDirectory.com")))
            //{
            //    var userStore = new UserStore<ApplicationUser>(context);
            //    var userManager = new UserManager<ApplicationUser>(userStore);
            //    var userToInsert = new ApplicationUser { UserName = "user@employeeDirectory.com" };
            //    userManager.Create(userToInsert, "user1!");
            //}

            var employees = new List<Employee>
            {
            new Employee{ID=1,FirstMidName="Earl",LastName="Andrews", Email="earljandrews@yahoo.com", EmployeeLocationID=1, JobTitle="HR Manager", PhoneNumber="1-800-Dev-Earl", PhoneNumberAlt="1-800-Dev-1111"},
            new Employee{ID=2,FirstMidName="Max",LastName="Ault", Email="Max@yahoo.com", EmployeeLocationID=2, JobTitle="Sales Representative", PhoneNumber="1-800-Max-Help", PhoneNumberAlt="1-800-Dev-1111"},
            new Employee{ID=3,FirstMidName="Frank",LastName="Andrews", Email="Frank@yahoo.com", EmployeeLocationID=3, JobTitle="Developer", PhoneNumber="1-800-DeF-Rank", PhoneNumberAlt="1-800-Dev-1111"},
            new Employee{ID=4,FirstMidName="Bob",LastName="Richardson", Email="Bob@yahoo.com", EmployeeLocationID=4, JobTitle="Bean Counter", PhoneNumber="1-800-Bob-Help", PhoneNumberAlt="1-800-Dev-1111"},
            new Employee{ID=5,FirstMidName="Johnathon",LastName="Lalande", Email="Johnathon@yahoo.com", EmployeeLocationID=5, JobTitle="N/A", PhoneNumber="1-800-Dev-John", PhoneNumberAlt="1-800-Dev-1111"}
           
            };

            employees.ForEach(s => context.Employees.AddOrUpdate(s));
            context.SaveChanges();

            var EmployeeLocations = new List<EmployeeLocation>
            {
            new EmployeeLocation{EmployeeLocationID=1,Location="Human Resources"},
            new EmployeeLocation{EmployeeLocationID=2,Location="Marketing"},
            new EmployeeLocation{EmployeeLocationID=3,Location="Developement"},
            new EmployeeLocation{EmployeeLocationID=4,Location="Finance"},
            new EmployeeLocation{EmployeeLocationID=5,Location="Black Ops"}
            };
            EmployeeLocations.ForEach(s => context.EmployeeLocations.AddOrUpdate(s));
            context.SaveChanges();
        }



    }
}