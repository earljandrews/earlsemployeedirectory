﻿using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace EmployeeDirectory.DAL
{
    public class CompanyConfiguration : DbConfiguration
    {
        public CompanyConfiguration()
        {
            SetExecutionStrategy("System.Data.SqlClient", () => new SqlAzureExecutionStrategy());
        }
    }
}