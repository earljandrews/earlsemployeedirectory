﻿using System;
using System.Collections.Generic;
using EmployeeDirectory.Models;
using System.Threading.Tasks;

namespace EmployeeDirectory.DAL
{
    public interface ICompanyRepository : IDisposable
    {
        IEnumerable<Employee> GetEmployees();
        IEnumerable<EmployeeLocation> GetEmployeeLocations();
        Task<Employee> GetEmployeeByIDAsync(int? ID);
        Employee GetEmployeeByID(int? ID);
        void InsertEmployee(Employee employee);
        void DeleteEmployee(int ID);
        void UpdateEmployee(Employee employee);
        void Save();
    }
}