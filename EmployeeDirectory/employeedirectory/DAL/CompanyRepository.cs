﻿using System;
using System.Collections.Generic;
using System.Linq;
using EmployeeDirectory.Models;
using System.Data;
using System.Data.Entity;
using System.Threading.Tasks;

namespace EmployeeDirectory.DAL
{
    public class CompanyRepository : ICompanyRepository
    {
        private CompanyContext _context;

        public CompanyRepository(CompanyContext companyContext)
        {
            this._context = companyContext;
        }

        public IEnumerable<Employee> GetEmployees()
        {
            return _context.Employees.ToList();
        }

        public async Task<Employee> GetEmployeeByIDAsync(int? ID)
        {
            return await _context.Employees.FindAsync(ID);
        }

        public Employee GetEmployeeByID(int? ID)
        {
            return _context.Employees.Find(ID);
        }

        public void InsertEmployee(Employee employee)
        {
            _context.Employees.Add(employee);
        }

        public void DeleteEmployee(int ID)
        {
            Employee employee = _context.Employees.Find(ID);
            _context.Employees.Remove(employee);
        }

        public void UpdateEmployee(Employee employee)
        {
            _context.Entry(employee).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<EmployeeLocation> GetEmployeeLocations()
        {
            return _context.EmployeeLocations.ToList();
        }
    }
}