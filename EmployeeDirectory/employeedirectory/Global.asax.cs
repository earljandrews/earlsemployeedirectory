﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using EmployeeDirectory.DAL;
using System.Data.Entity.Infrastructure.Interception;
using EmployeeDirectory.Migrations;
using System.Data.Entity;

namespace EmployeeDirectory
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(
              new MigrateDatabaseToLatestVersion<CompanyContext, EmployeeDirectory.Migrations.Configuration>()
              );


            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
