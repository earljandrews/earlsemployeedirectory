﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeDirectory.Models
{
    public class EmployeeLocation
    {
        [Key]
        public int EmployeeLocationID { get; set; }

        [StringLength(50)]
        [Display(Name = "Location")]
        public string Location { get; set; }


    }
}