﻿$(document).ready(function () {
    
    $('body').on('click', '.DeleteEmployeeButton', function () {
        var str = this.id;
        var res = str.split('_');
        var EmployeeID = res[1];

        $.ajax({
            url: '@Url.Action("Delete", "Employee")',
            data: { 'id': EmployeeID },
            type: "post",
            cache: false,
            success: function (savingStatus) {
                // Display a success toast, with a title
                //toastr.success('Your Employee has been deleted', 'Deleted');
                //$('#urlEmployeepartialViewDiv_' + category).load('/Category/UrlEmployees/' + category);

            },
            error: function (xhr, ajaxOptions, thrownError) {
                // Display an error toast, with a title
                //toastr.error('Delete failed, contact your system administrator.', 'Error!');
            }
        });
    });

});