﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EmployeeDirectory.Models;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeDirectory.ViewModels
{
    public class EmployeeViewModel
    {
        public int ID { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "First Name")]
        public string FirstMidName { get; set; }

        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        [Display(Name = "Location ID")]
        public int EmployeeLocationID { get; set; }

        [Display(Name = "Location")]
        public string EmployeeLocationText { get; set; }

        public string Email { get; set; }

        [Display(Name = "Phone #")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Alt Phone #")]
        public string PhoneNumberAlt { get; set; }

        public int SelectedEmployeeLocationId { get; set; }

        public IEnumerable<SelectListItem> EmployeeLocations { get; set; }
    }


}