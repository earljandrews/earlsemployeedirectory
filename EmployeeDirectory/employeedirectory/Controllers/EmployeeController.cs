﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EmployeeDirectory.DAL;
using EmployeeDirectory.Models;
using EmployeeDirectory.ViewModels;
using PagedList;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace EmployeeDirectory.Controllers
{
    [Authorize]
    public class EmployeeController : Controller
    {
       
        private ICompanyRepository _companyRepository;


        public EmployeeController()
        {
            this._companyRepository = new CompanyRepository(new CompanyContext());
        }

        public EmployeeController(ICompanyRepository companyRepository)
        {
              this._companyRepository = companyRepository;
        }


        // GET: Employee
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var employees = _companyRepository.GetEmployees();

            if (!String.IsNullOrEmpty(searchString))
            {
                employees = employees.Where(s => s.LastName.Contains(searchString)
                                       || s.FirstMidName.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    employees = employees.OrderByDescending(s => s.LastName);
                    break;
                default:  // Name ascending 
                    employees = employees.OrderBy(s => s.LastName);
                    break;
            }

            int pageSize = 3;
            int pageNumber = (page ?? 1);

            var models = GetEmployeeViewModels(employees);
            return View(models.ToPagedList(pageNumber, pageSize));
        }


        // GET: Employee/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = await _companyRepository.GetEmployeeByIDAsync(id);
            if (employee == null)
            {
                return HttpNotFound();
            }

            var model = GetEmployeeViewModel(employee);
            return View( model);

        }

        // GET: Employee/Create
        [Authorize(Roles="Admin")]
        public ActionResult Create()
        {
            var employee = new Employee();
            var model = GetEmployeeViewModel(employee);
            return View(model);
        }

        // POST: Employee/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LastName, FirstMidName, JobTitle, EmployeeLocationID, Email, PhoneNumber, PhoneNumberAlt")]EmployeeViewModel employeeViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var employeeToUpdate = new Employee();

                    employeeToUpdate.Email = employeeViewModel.Email;
                    employeeToUpdate.EmployeeLocationID = employeeViewModel.EmployeeLocationID;
                    employeeToUpdate.FirstMidName = employeeViewModel.FirstMidName;
                    employeeToUpdate.JobTitle = employeeViewModel.JobTitle;
                    employeeToUpdate.LastName = employeeViewModel.LastName;
                    employeeToUpdate.PhoneNumber = employeeViewModel.PhoneNumber;
                    employeeToUpdate.PhoneNumberAlt = employeeViewModel.PhoneNumberAlt;
                    _companyRepository.InsertEmployee(employeeToUpdate);
                    _companyRepository.Save();

                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View(employeeViewModel);
        }


        // GET: Employee/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = _companyRepository.GetEmployeeByID(id);
            if (employee == null)
            {
                return HttpNotFound();
            }

            var model = GetEmployeeViewModel(employee);

            return View(model);

        }

        // POST: Employee/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id, [Bind(Include = "LastName, FirstMidName, JobTitle, EmployeeLocationID, Email, PhoneNumber, PhoneNumberAlt")]EmployeeViewModel employeeViewModel)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }



            if (ModelState.IsValid)
            {
                Employee employeeToUpdate = _companyRepository.GetEmployeeByID(id);
                employeeToUpdate.Email = employeeViewModel.Email;
                employeeToUpdate.EmployeeLocationID = employeeViewModel.EmployeeLocationID;
                employeeToUpdate.FirstMidName = employeeViewModel.FirstMidName;
                employeeToUpdate.JobTitle = employeeViewModel.JobTitle;
                employeeToUpdate.LastName = employeeViewModel.LastName;
                employeeToUpdate.PhoneNumber = employeeViewModel.PhoneNumber;
                employeeToUpdate.PhoneNumberAlt = employeeViewModel.PhoneNumberAlt;
               _companyRepository.Save();

            }

            return RedirectToAction("Index");

        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Delete failed. Try again, and if the problem persists see your system administrator.";
            }
            Employee employee = _companyRepository.GetEmployeeByID(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employee/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                _companyRepository.DeleteEmployee(id);
                _companyRepository.Save();
            }
            catch (RetryLimitExceededException/* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }



        private List<EmployeeViewModel> GetEmployeeViewModels(IEnumerable<Employee> employees)
        {
            List<EmployeeViewModel> result = new List<EmployeeViewModel>();
            foreach (var item in employees)
            {
                var employeeViewModel = new EmployeeViewModel();
                employeeViewModel = GetEmployeeViewModel(item);
                result.Add(employeeViewModel);
            }
            return result;
        }

        private EmployeeViewModel GetEmployeeViewModel(Employee employee)
        {
            int myEmployeeLocationID = 1;
            if (employee.EmployeeLocationID > 0)
            {
                myEmployeeLocationID = employee.EmployeeLocationID;
            }
            var employeeLocations = _companyRepository.GetEmployeeLocations();
            EmployeeLocation myEmployeeLocation = employeeLocations.Single(s => s.EmployeeLocationID == myEmployeeLocationID);
            string employeeLocationText = myEmployeeLocation.Location;
          
            var model = new EmployeeViewModel
            {
                ID = employee.ID,
                LastName = employee.LastName,
                FirstMidName = employee.FirstMidName,
                JobTitle = employee.JobTitle,
                EmployeeLocationID = employee.EmployeeLocationID,
                EmployeeLocationText = employeeLocationText,
                Email = employee.Email,
                PhoneNumber = employee.PhoneNumber,
                PhoneNumberAlt = employee.PhoneNumberAlt,
                SelectedEmployeeLocationId = employee.EmployeeLocationID,
                EmployeeLocations = employeeLocations.Select(x => new SelectListItem
                {
                    Value = x.EmployeeLocationID.ToString(),
                    Text = x.Location
                })
            };

            return model;
        }
    }
}
